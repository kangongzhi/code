import React, { Component } from "react";
// types
/*************************************************************/
interface Props {
  error: string;
  title: string;
  children: any;
}
interface State {
  hasError: boolean;
}

export const Error = ({ error = "", title = "" }) => (
  <div>
    {title}: {error}
  </div>
);
// component
/*************************************************************/
class ErrorBoundary extends Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch() {
    this.setState({
      hasError: true
    });
  }

  render() {
    const { error, title } = this.props;

    if (this.state.hasError === true) {
      // Error path
      return <Error title={title} error={error} />;
    }
    // Normally, just render children
    return this.props.children;
  }
}

export default ErrorBoundary;
