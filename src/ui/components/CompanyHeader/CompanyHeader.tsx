import React from "react";
import { CompanyInfo } from "../../../redux/company-info";
import styles from "./Company.module.scss";

interface Props {
  companyInfo: CompanyInfo;
}
const CompanyHeader = ({
  companyInfo: { companyEst, companyMotto, companyName }
}: Props) => (
  <header className={styles.header}>
    <div className={styles.left}>
      <h3 className={styles.name}>{companyName}</h3>
      <p className={styles.motto}>{companyMotto}</p>
    </div>
    <p className={styles.est}>since {companyEst}</p>
  </header>
);

export default CompanyHeader;
