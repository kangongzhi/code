import styled from "styled-components";

const ProfileImage = styled.img`
  width: 128px;
  border-radius: 5px;
`;

export default ProfileImage;
