import React from "react";
import styles from "./Modal.module.scss";
// helpers
/*************************************************************/
const CrossBtn = () => (
  <svg
    aria-labelledby="title-icon-close"
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    focusable="false"
  >
    <title id="title-icon-close" lang="en">
      Close
    </title>
    <polygon
      fill="#ffffff"
      className="icons-background"
      points="5.414 4 12 10.586 18.586 4 20 5.414 13.414 12 20 18.586 18.586 20 12 13.414 5.414 20 4 18.586 10.586 12 4 5.414"
    ></polygon>
  </svg>
);
// component
/*************************************************************/
const Modal = ({ children, onClose, isOpen }) => (
  <div
    className={`${styles.modal} ${isOpen === true ? styles.show : styles.hide}`}
  >
    <div className={styles.contentContainer}>
      <div className={styles.closeBtn} onClick={onClose}>
        <CrossBtn />
      </div>
      <div className={styles.content}>{children}</div>
    </div>
  </div>
);

export default Modal;
