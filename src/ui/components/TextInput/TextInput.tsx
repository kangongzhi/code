import styled from "styled-components";

const TextInput = styled.input`
  width: 100%;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  line-height: 1.5;
  font-weight: 400;
  color: #2d373e;
  background-color: #fff;
  border: 1px solid #8c9296;
  border-radius: 0.1875rem;
  -webkit-transition: border 0.2s ease;
  transition: border 0.2s ease;
  padding: 0.3125rem 0.75rem;
  font-size: 1rem;
  height: 1.55rem;
`;

export default TextInput;
