import styled from "styled-components";

const Select = styled.select`
  background-image: url("data:image/svg+xml;charset%3DUS-ASCII,%3Csvg%20aria-labelledby%3D%22title-icon-caret%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2214%22%20height%3D%228%22%20viewBox%3D%220%200%2014%208%22%3E%20%20%3Ctitle%20id%3D%22title-icon-caret%22%20lang%3D%22en%22%3ECaret%3C%2Ftitle%3E%20%20%3Cpolygon%20fill%3D%22%23575f65%22%20class%3D%22icons-background%22%20fill-rule%3D%22evenodd%22%20points%3D%220%200%207%208%2014%200%22%2F%3E%3C%2Fsvg%3E");
  background-repeat: no-repeat;
  background-position: right 0.75rem center;
  padding-right: 2.125rem;
  display: block;
  width: 100%;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  line-height: 1.5;
  font-weight: 400;
  color: #2d373e;
  background-color: #fff;
  border: 1px solid #8c9296;
  border-radius: 0.1875rem;
  -webkit-transition: border 0.2s ease;
  transition: border 0.2s ease;
  font-size: 1rem;
  height: 2.25rem;
`;

export default Select;
