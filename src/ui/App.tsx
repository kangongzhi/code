import React, { lazy, Suspense } from "react";
import { Route, Switch } from "react-router-dom";
import Loader from "./components/Loader/Loader";
import ErrorBoundary from "./components/ErrorBoundary/ErrorBoundary";
// code splitting
/*************************************************************/
const loadHomeContainer = () => import("./pages/Home/HomeContainer");
const loadNotFound = () => import("./pages/NotFound/NotFound");

const HomeContainer = lazy(loadHomeContainer);
const NotFound = lazy(loadNotFound);
// component
/*************************************************************/
const App = () => (
  <div>
    <main>
      <ErrorBoundary
        error={
          "Something went wrong, please try refresh the page or contact the support team"
        }
        title={"Javascript error"}
      >
        <Suspense fallback={<Loader />}>
          <Switch>
            <Route exact path={"/"} component={HomeContainer} />
            <Route component={NotFound} />
          </Switch>
        </Suspense>
      </ErrorBoundary>
    </main>
  </div>
);

export default App;
