import React from "react";
import { Employee } from "../../../../redux/employees";
import EmployeeCardContainer from "./EmployeeCard/EmployeeCardContainer";
import styles from "./EmployeeList.module.scss";

interface Props {
  employees: Employee[];
}
const EmployeeList = ({ employees }: Props) => (
  <div className={styles.list}>
    {employees.map(employee => (
      <EmployeeCardContainer employee={employee} key={employee.id} />
    ))}
  </div>
);

export default EmployeeList;
