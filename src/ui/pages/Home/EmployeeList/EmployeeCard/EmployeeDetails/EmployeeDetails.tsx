import React from "react";
import { Employee } from "../../../../../../redux/employees";
import { emptyString } from "../../../../../../helper";
import styles from "./EmployeeDetails.module.scss";
import ProfileImage from "../../../../../components/ProfileImage/ProfileImage";
// helpers
/*************************************************************/
const defaultEmployee = {
  id: emptyString,
  avatar: emptyString,
  firstName: emptyString,
  lastName: emptyString,
  jobTitle: emptyString,
  age: null,
  bio: emptyString,
  dateJoined: emptyString
};
// component
/*************************************************************/
interface Props {
  employee: Employee;
}
const EmployeeDetails = ({ employee = defaultEmployee }: Props) => {
  const {
    firstName,
    age,
    avatar,
    bio,
    dateJoined,
    jobTitle,
    lastName
  } = employee;
  return (
    <div className={styles.details}>
      <div className={styles.left}>
        <ProfileImage src={avatar} />
        <p>{jobTitle}</p>
        <p>{age}</p>
        <p>{dateJoined}</p>
      </div>
      <div className={styles.right}>
        <div className={styles.name}>{`${firstName} ${lastName}`}</div>
        <p>{bio}</p>
      </div>
    </div>
  );
};

export default EmployeeDetails;
