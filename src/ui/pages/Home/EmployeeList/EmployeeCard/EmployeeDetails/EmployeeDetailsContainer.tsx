import React from "react";
import { connect } from "react-redux";
import EmployeeDetails from "./EmployeeDetails";
import { Employee } from "../../../../../../redux/employees";
// component
/*************************************************************/
interface Props {
  employee: Employee;
}
const EmployeeDetailsContainer = ({ employee }: Props) => (
  <EmployeeDetails employee={employee} />
);
// connect to store
/*************************************************************/
const mapStateToProps = ({ selectedEmployeeId, employees }) => ({
  employee: employees.find(({ id }) => id === selectedEmployeeId)
});

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeeDetailsContainer);
