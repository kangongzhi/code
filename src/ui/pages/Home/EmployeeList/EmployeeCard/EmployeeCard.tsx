import React from "react";
import { Employee } from "../../../../../redux/employees";
import styles from "./EmployeeCard.module.scss";
import ProfileImage from "../../../../components/ProfileImage/ProfileImage";

interface Props {
  employee: Employee;
  onClick;
}
const EmployeeCard = ({
  employee: { id, bio, avatar, firstName, lastName },
  onClick
}: Props) => (
  <div className={styles.card} onClick={onClick(id)}>
    <ProfileImage src={avatar} />
    <div className={styles.info}>
      <div className={styles.name}>{`${firstName} ${lastName}`}</div>
      <div>{bio}</div>
    </div>
  </div>
);

export default EmployeeCard;
