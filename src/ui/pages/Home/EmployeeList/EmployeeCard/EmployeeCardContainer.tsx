import React, { useState } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Employee } from "../../../../../redux/employees";
import EmployeeCard from "./EmployeeCard";
import { updateSelectedEmployeeId } from "../../../../../redux/selected-employee-id";
import Modal from "../../../../components/Modal/Modal";
import EmployeeDetailsContainer from "./EmployeeDetails/EmployeeDetailsContainer";
// helpers
/*************************************************************/
const closed = false;
const opened = true;

const handleClick = (updateSelectedEmployeeId, setIsOpen) => id => () => {
  updateSelectedEmployeeId(id);
  setIsOpen(opened);
};

const closeModal = setIsOpen => () => {
  setIsOpen(closed);
};
// component
/*************************************************************/
interface Props {
  employee: Employee;
  updateSelectedEmployeeId;
}
const EmployeeCardContainer = ({
  employee,
  updateSelectedEmployeeId
}: Props) => {
  const [isOpen, setIsOpen] = useState(closed);
  return (
    <div>
      <EmployeeCard
        employee={employee}
        onClick={handleClick(updateSelectedEmployeeId, setIsOpen)}
      />
      <Modal isOpen={isOpen} onClose={closeModal(setIsOpen)}>
        <EmployeeDetailsContainer />
      </Modal>
    </div>
  );
};
// connect to store
/*************************************************************/
const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateSelectedEmployeeId
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EmployeeCardContainer);
