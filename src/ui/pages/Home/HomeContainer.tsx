import React, { useEffect } from "react";
import Home from "./Home";
import { getInitData, Response } from "../../../apis/get-init-data";
import { CompanyInfo, loadCompanyInfo } from "../../../redux/company-info";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Employee, loadEmployees } from "../../../redux/employees";
import { parseDate } from "../../../helper";
// helpers
/*************************************************************/
const saveToReduxStore = (
  loadCompanyInfo,
  loadEmployees,
  parseCompany,
  parseEmployees
) => ({ companyInfo, employees }: Response) => {
  loadCompanyInfo(parseCompany(companyInfo));
  loadEmployees(parseEmployees(employees));
};

const init = (getInitData, saveToReduxStore) => () => {
  getInitData().then(saveToReduxStore);
};

const parseEmployee = ({ dateJoined, ...rest }: Employee) => ({
  ...rest,
  dateJoined: parseDate(dateJoined)
});

const parseCompany = ({ companyEst, ...rest }: CompanyInfo) => ({
  ...rest,
  companyEst: parseDate(companyEst)
});

const parseEmployees = parseEmployee => (employees: Employee[]) =>
  employees.map(parseEmployee);
// component
/*************************************************************/
const HomeContainer = ({
  loadCompanyInfo,
  loadEmployees,
  companyInfo,
  employees
}) => {
  useEffect(
    init(
      getInitData,
      saveToReduxStore(
        loadCompanyInfo,
        loadEmployees,
        parseCompany,
        parseEmployees(parseEmployee)
      )
    ),
    []
  );

  return <Home companyInfo={companyInfo} employees={employees} />;
};

// connect to store
/*************************************************************/
const mapStateToProps = ({ companyInfo, employees }) => ({
  companyInfo,
  employees
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      loadCompanyInfo,
      loadEmployees
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
