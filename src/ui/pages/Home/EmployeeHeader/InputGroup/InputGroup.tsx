import React from "react";
import styles from "./InputGroup.module.scss";

const InputGroup = ({ label, input }) => (
  <div className={styles.group}>
    <div className={styles.label}>{label}</div>
    <div className={styles.input}>{input}</div>
  </div>
);

export default InputGroup;
