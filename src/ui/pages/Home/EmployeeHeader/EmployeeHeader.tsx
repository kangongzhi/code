import React from "react";
import styles from "./EmployeeHeader.module.scss";
import InputGroup from "./InputGroup/InputGroup";
import Select from "../../../components/Select/Select";
import TextInput from "../../../components/TextInput/TextInput";

const EmployeeHeader = () => (
  <div className={styles.header}>
    <div className={styles.content}>
      <div className={styles.title}>Our Employees</div>
      <div className={styles.groups}>
        <InputGroup label={"Sort by: "} input={<Select />} />
        <InputGroup label={"Search "} input={<TextInput />} />
      </div>
    </div>
    <hr className={styles.hr} />
  </div>
);

export default EmployeeHeader;
