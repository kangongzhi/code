import React from "react";
import { CompanyInfo } from "../../../redux/company-info";
import { Employee } from "../../../redux/employees";
import CompanyHeader from "../../components/CompanyHeader/CompanyHeader";
import EmployeeList from "./EmployeeList/EmployeeList";
import EmployeeHeader from "./EmployeeHeader/EmployeeHeader";
import styles from "./Home.module.scss";

interface Props {
  companyInfo: CompanyInfo;
  employees: Employee[];
}
const Home = ({ companyInfo, employees }: Props) => (
  <div>
    <CompanyHeader companyInfo={companyInfo} />
    <div className={styles.content}>
      <EmployeeHeader />
      <div className={styles.list}>
        <EmployeeList employees={employees} />
      </div>
    </div>
  </div>
);

export default Home;
