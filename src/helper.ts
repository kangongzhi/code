export const emptyString = "";
export const parseDate = (date: string) => {
  const newDate = new Date(date);
  return `${newDate.getDate()}/${newDate.getMonth()}/${newDate.getFullYear()}`;
};
