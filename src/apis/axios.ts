import axios from "axios";

const instance = axios.create();
// interceptors
/*************************************************************/
const resResolveInterceptor = ({ data }: any) => data;
const resRejectInterceptor = (err: any) => Promise.reject(err);

instance.interceptors.response.use(resResolveInterceptor, resRejectInterceptor);

export default instance;
