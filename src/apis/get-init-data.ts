import axios from "./axios";
// response type
/*************************************************************/
interface CompanyInfo {
  companyName: string;
  companyMotto: string;
  companyEst: string;
}

interface Employee {
  id: string;
  avatar: string;
  firstName: string;
  lastName: string;
  jobTitle: string;
  age: number;
  bio: string;
  dateJoined: string;
}

export interface Response {
  companyInfo: CompanyInfo;
  employees: Employee[];
}
// url
/*************************************************************/
const url = `${process.env.PUBLIC_URL}/mock/sample-data.json`;
// api
/*************************************************************/
export const getInitData = (): Promise<Response> => axios.get(url);
