import { emptyString } from "../helper";
// actions
/*************************************************************/
const UPDATE = "@selected-employee/update";
// actions creators
/*************************************************************/
export const updateSelectedEmployeeId = (employeeId: string) => ({
  type: UPDATE,
  employeeId
});
// reducer
/*************************************************************/
const initialState = emptyString;

const selectedEmployeeId = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE:
      return action.employeeId;
    default:
      return state;
  }
};

export default selectedEmployeeId;
