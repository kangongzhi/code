// actions
/*************************************************************/
const LOAD = "@employees/load";
// actions creators
/*************************************************************/
export interface Employee {
  id: string;
  avatar: string;
  firstName: string;
  lastName: string;
  jobTitle: string;
  age: number;
  bio: string;
  dateJoined: string;
}

export const loadEmployees = (employees: Employee[] = []) => ({
  type: LOAD,
  employees
});
// reducer
/*************************************************************/
const initialState = [];

const employees = (state = initialState, action) => {
  switch (action.type) {
    case LOAD:
      return [...action.employees];
    default:
      return state;
  }
};

export default employees;
