// actions
/*************************************************************/
const OPEN = "@modal/open";
const CLOSE = "@modal/close";
// actions creators
/*************************************************************/
export const openModal = () => ({
  type: OPEN
});

export const closeModal = () => ({
  type: CLOSE
});
// reducer
/*************************************************************/
const isOpen = true;
const isClosed = false;

const initialState = isClosed;

const companyInfo = (state = initialState, action) => {
  switch (action.type) {
    case OPEN:
      return isOpen;
    case CLOSE:
      return isClosed;
    default:
      return state;
  }
};

export default companyInfo;
