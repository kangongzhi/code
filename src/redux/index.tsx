import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import companyInfo from "./company-info";
import employees from "./employees";
import selectedEmployeeId from "./selected-employee-id";

const reducers = { companyInfo, employees, selectedEmployeeId };

const createRootReducer = (history: any) =>
  combineReducers({ router: connectRouter(history), ...reducers });

export default createRootReducer;
// export for unit testing
/*****************************************/
export { reducers };
