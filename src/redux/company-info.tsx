import { emptyString } from "../helper";
// actions
/*************************************************************/
const LOAD = "@company-info/load";
// actions creators
/*************************************************************/
export interface CompanyInfo {
  companyName: string;
  companyMotto: string;
  companyEst: string;
}

export const loadCompanyInfo = (companyInfo: CompanyInfo) => ({
  type: LOAD,
  companyInfo
});
// reducer
/*************************************************************/
const initialState = {
  companyName: emptyString,
  companyMotto: emptyString,
  companyEst: emptyString
};

const companyInfo = (state = initialState, action) => {
  switch (action.type) {
    case LOAD:
      return { ...action.companyInfo };
    default:
      return state;
  }
};

export default companyInfo;
